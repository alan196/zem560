#!/usr/bin/env python

from datetime import datetime
from struct import pack, unpack
import zkem.zkem as zkem

# Put IP address of clock here
SERVER = ''

term = zkem.zem560()
if(term.connect(SERVER)):
    time = term.get_time()

    if(time):
        print 'Time on clock is: %s' % time
    else:
        print 'Problem accoured, could not get time.'

    term.disconnect()

else:
    print 'Failed to connect to %s' % SERVER
