#!/usr/bin/env python

import sys
from datetime import datetime
from struct import pack, unpack
import zkem.zkem as zkem

# Put IP address of clock here
SERVER = ''

print 'This will set the current system time on the clock.'
answer = raw_input('Type YES to set the time: ')

if answer != 'YES':
    print 'Stopping...'
    sys.exit()

term = zkem.zem560()
if(term.connect(SERVER)):
    if(term.set_time(datetime.now())):
        print 'Time set'
    else:
        print 'Problem accoured, time not set'

    term.disconnect()

else:
    print 'Failed to connect to %s' % SERVER
